$(document).ready(function(){
    $(".isotope-container").isotope({});
    
    $("#isotope-filters").on("click","button",function(){
        let filtervalue = $(this).attr("data-filter");
        console.log(filtervalue);
        
        $("#isotope-container").isotope({
            filter:filtervalue
        });
        $("#isotope-filters").find('.active').removeClass('active');
    
    $(this).addClass('active');
})

});

$(document).ready(function(){
  $("#team").owlCarousel({
      items:3,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:false,
      navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>'],
      
  });
});

$(document).ready(function(){
  $(".testimonials").owlCarousel({
      items:1,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:true,
//      navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>'],
      
  });
});